using System.Collections.Generic;
using api.example.Models;

namespace api.example.Interfaces
{
    // STEP 2: Create the interface for repository. This step is for ensure availability for unit testing.

    public interface IBooksRepository
    {
        IEnumerable<BooksDTO> getBooks();
        BooksDTO getBookById(int id);
    }
}