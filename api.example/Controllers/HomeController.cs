﻿using Microsoft.AspNetCore.Mvc;

// This controller is optional, it was created only to return a default message when a request is made direct to the domain.

namespace api.example.Controllers
{
    [Route("/")]
    public class HomeController : Controller
    {
        [HttpGet]
        public IActionResult Get()
        {
            return Ok("API is ALIVE!!");
        }
    }
}
