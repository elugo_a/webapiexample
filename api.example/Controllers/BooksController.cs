﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api.example.Repository;
using api.example.Models;
using api.example.Interfaces;

namespace api.example.Controllers
{
    // STEP 4: Create the controller.

    [Route("api/[controller]")]
    public class BooksController : Controller
    {
        IBooksRepository _repository;

        public BooksController(IBooksRepository repository)
        {
            _repository = repository;
        }

        // GET api/books
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_repository.getBooks());
        }

        // GET api/books/<id>
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            return Ok(_repository.getBookById(id));
        }

        // POST api/books
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/books/<id>
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/books/<id>
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
