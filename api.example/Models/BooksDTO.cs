namespace api.example.Models
{
    // STEP 1: Create the Data Transfer Object (DTO).

    public class BooksDTO
    {
        public int id { get; set; }
        public string title { get; set; }
    }
}