
using System.Collections.Generic;
using api.example.Models;
using System.Linq;
using api.example.Interfaces;

namespace api.example.Repository
{
    // STEP 3: Create the repository, inheriting from the interface.

    public class BooksRepository : IBooksRepository
    {
        // This is our data source (instead of a server).
        public List<BooksDTO> lBooks { get; set; } = new List<BooksDTO> {
            new BooksDTO { id = 1, title = "Book 1"},
            new BooksDTO { id = 2, title = "Book 2"},
            new BooksDTO { id = 3, title = "Book 3"}
        };

        #region Methods to get the data
        public IEnumerable<BooksDTO> getBooks()
        {
            return lBooks.OrderBy(r => r.id).ToList();
        }

        public BooksDTO getBookById(int id)
        {
            return lBooks.Where(r => r.id == id).FirstOrDefault();
        }
        #endregion
    }
}