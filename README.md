## Web API Example
----

### This is an example of how to create a web API using dotNet Core.

If you are to open this project in **Visual Studio**, you don't worry about dependencies,
they are downloaded automatically.

If you are to open this project with **VSCode** you need to do what follows:

1. Install dependencias (if they were not installed automatically): Execute the next
instructions in VSCode Terminal:

    * ```dotnet add package Microsoft.AspNetCore```
    * ```dotnet add package Microsoft.AspNetCore.Mvc```

2. To compile and run the project excecute:

    * ```dotnet run```

If you want to be able to perform a test request from VSCode, you need to install the
"REST Client" plugin:

* _Go to "Extensions" and search for "REST client" and install the one published by
    "Huachao Mao"._

* Now in the "wwwroot" folder you will find a file named "get.http", open it and you'll
notice that above the request line there is this label with the ext "Send request",
then click on this label and you will be testing this API.
